#Javsacript State Object

This is a conversion of the Nooku model state object into pure javascript.

**BETA** be aware, this hasn't been tested in anything other than chrome.

##Usage

	//listener is optional, see the listeners section below
	var state = new State(listener); 
	
**Insert**:

	state.insert(name, filter, fallback, unique, required); //Only name is required
	
* **name** (string) - the state name, this can be any string value
* **filter** (string|function) - states are filtered when they are set. 5 default filteres are provided (int, float, string, array, object), or supply a function that has 1 argument
* **fallback** - this is the fallback value used when the state is reset
* **unique** (boolean) - defines if the state is unique
* **required** (array) - if more than 1 state is required to be considered unique, supply an array here (e.g. for a composite key)
	
**set**:
	
	//Sets the value of a state named "name". Setting callback to false suppresses the onStateChange callback
	state.set(name, value, callback); 
			
**get**:
	
	//Gets the value of a state named "name", using fallback if it doesn't exit
	state.get(name, fallback); 	

**getValues**:

	//Returns all the set values	
	state.getValues();
	
**setValues**:

	//Sets the values of all the states defined in the "object" argument. Setting callback to false suppresses the onStateChange callback
	state.setValues(object, callback) 

**isUnique**:
	
	//Returns true if the state is unique (i.e. a unique state is set with a value)
	state.isUnique()
	
**isEmpty**:
	
	//Returns true if no states have values, an optional array of state to exclude can be provided
	state.isEmpty(exclude); 	
	
**reset**:	

	//Resets (nulls) all state values unless fallback is set, in which case they are set to the fallback value defined against the state (when inserting)
	state.reset(fallbak); 	
	
**addListener**

	//Adds a listener to be called when the state is changed. Name restricts the listener to only be called when the named state changes, ommit this to be triggered on all state changes.
	state.addListener(listener, name);
	
**removeListener**

	//Removes a listener, either by matching the listener or the name (listener must be null if name supplied)
	state.removeListener(listener, name);
	
----
	
#jQuery State Binding

Also packaged with the Javscript State Object is a jQuery binding plugin for binding states to DOM elements. The idea behind state binding is that you will never need to set the value ro read the value of an individual element, all interation is done through the state. The state object can be shared and values read from different parts of your application, this could then be used to generate a query string for example.

Several basic HTML elements are supported by default, including:

* **Input** - All standard input boxes update the state on form submit
* **Textarea** - Textareas update the state on form submit
* **Select** - Select lists update the state on change
* **Checkbox** - Checkboxes update the state on change
* **Radio** - Radio buttons update the state on change
* **UL/OL/List** - List elements children (`<li>`) update the state on click
* **Button** - Buttons update the state on click, additionally has a class of "active" applied when state set to a value
* **Other** - other DOM elements are supported but do not trigger events, only respond to state changes
* **Reset** - Reset resets all or specific states on click

##Options/Data-attributes

State binding is configured by default through data attributes, some are required, some optional depending upon the element type.

###Required attributes:

* **data-state="ABC"** - Defines the state object being used. `ABC` can be one of several values:

	* `#id`, `.class`, `[selector]` - State is requested from .data('state') of the matching 
	* `this` - The state is requested from .data('state') of the element
	* `$name` - The state is requested from $(document).data(name), allows for globally registered states
	* `name` - The state is requested from window[name], allows for globally registered states

* **data-state-name="DEF"** - Defines the state name, where `DEF` is the state name. This can be the name of any state defined in the state object. If the state is not defined, a new state with no filter will be defined in the state object. Note, for form inputs, this value is optional as the "name" attribute will be used.
* * **data-state-reset** - Applies to `reset` buttons only. This is a required attribute. A comma separated list of states to be reset when the button is clicked can optionally be set, e.g. `data-state-reset="limit,offset"`, if no list is supplied, then all states are reset.


###Optional attributes:

* **data-state-event** - Defines the DOM event that the element will respond to. This can by any valid DOM event.
* **data-state-type** - The binding type can be overridden to make it behave like another binding. This can be used to force the binding to a particular type, for example, `list` might be specified when binding the state to a `div` element to force it to behave like a list.
* **data-state-disable-null** - Empty strings will be converted to null when setting the state, defaults to false, set to true to disable this functionality. This is because when reading values of elements, empty values are interpreted as an empty string.
* **data-state-selector** - Applies to `list` elements only, defines the child selector. Defaults to `> *`, effectively selecting all children of the element.
* **data-state-value** - Applies only to the children of `list` elements, this specifies the state value that will be set for the parents' state when the element is clicked.
* **data-state-use-value** - Defaults to true, initializes the state with the element value, set to 0 to disable

##How to use:

	<script type="text/javascript">
		var state = new State();
		state.insert('search','string');
	
		$(document).data('my_state', state);
	</script>

	<form>
		<input type="text" name="search" value="original state value" data-state="$my_state" />
	</form>
	
	<script type="text/javascript">
		setTimeout(function(){
		
			$(document).data('my_state').set('search','my search param');
		
		}, 3000);
	</script>
	
The above with bind the text input to the `search` value stored in `$(document).data('my_state')` and after 3 seconds the value will change to `my search param`. Try experimenting with this in your console, or look at the larger example in the test.html file.

