(function($){

    "use strict"; // jshint ;

    /**
     * State object
     * @param element - The element being bound
     * @param options (optional) - Options object
     * @constructor
     */
    var StateBinding = function(element, options){
        this.construct(element, options);
    }

    StateBinding.prototype = {

        constructor: StateBinding,

        element: null,
        state: null,
        options: {
            name: null,
            event: null,
            useValue: true,
            selector: null,
            reset: null,
            state: '',
            disableNull: null,
            binders: {}
        },
        tagName: null,
        tagType: null,
        binders: {
            /**
             * Binds the state to an input/textarea/select element
             * @returns {*}
             */
            input: function(){

                var type = this.options.type || this.tagName;
                var element = this.element, event = this.options.event;

                //Set the initial state value
                if(this.element.val() && this.options.useValue) this.setStateValue(this.element.val(), false, true);
                else this.element.val(this.getStateValue());

                //If no event set, determine event
                if(!event){
                    switch(type){
                        case 'INPUT':
                        case 'TEXTAREA':
                            if(this.element[0].form) event = 'submit';
                            break;

                        case 'SELECT':
                            event = 'change';
                            break;

                        case 'RADIO':
                        case 'CHECKBOX':
                            event = 'checked';
                            break;
                    }
                }

                //Submit events must be bound to the form
                if(event == 'submit') element = $(this.element[0].form);
                event += '.state.data-api';

                //Add event to element
                element.on(event, $.proxy(function(e){
                    e.preventDefault();
                    var val = this.element.val();

                    this.setStateValue(val);
                }, this))

                //Set element value on state change
                this.getState().addListener($.proxy(function(name, value){

                    this.element.val(value);
                }, this), this.options.name);

                return this;
            },

            /**
             * Binds the state to a checkbox element
             * @returns {*}
             */
            checkbox: function(){
                var event = (this.options.event || 'change');

                var values = this.getStateValue() || [];

                //Gets the input value and converts to int/float as appropriate
                var getValue = $.proxy(function(element){
                    var value = element ? element.val() : this.element.val();
                    if(!isNaN(parseFloat(value))) value = parseFloat(value);
                    else if(!isNaN(parseInt(value, 10))) value = parseInt(value, 10);
                    return value;
                }, this);

                //Set state default
                if(this.element.is(':checked') && this.options.useValue){
                    values.push(getValue());
                    this.setStateValue(values, false, true);
                }else if(values.indexOf(getValue()) != -1){
                    this.element.prop('checked','checked');
                }

                //Respond to change event
                this.element.on(event, $.proxy(function(){
                    values = this.getStateValue();
                    var value = getValue();
                    var index = values.indexOf(value);

                    if(this.element.is(':checked')){
                        if(index === -1) values.push(value)
                    }else if(index !== -1){
                        delete values[index];
                        values = values.filter(function (item) { return item != undefined })
                    }

                    this.setStateValue(values);
                }, this));

                //Find the parent, in most cases this will be the form
                var parent = this.element[0].form ? $(this.element[0].form) : this.element.parent();

                //On state change, find all other checkboxes and verify if they're in the value array
                this.getState().addListener($.proxy(function(name, value){

                    parent.find('input[name="'+this.element.prop('name')+'"][type=checkbox]').each(function(){
                        $(this).prop('checked', value ? value.indexOf(getValue($(this))) !== -1 : false);
                    })
                }, this), this.options.name);

                return this;
            },

            /**
             * Binds the state to a radio element
             * @returns {*}
             */
            radio: function(){
                var event = (this.options.event || 'change')+'.state.data-api';

                //Set state default
                if(this.element.is(':checked') && this.options.useValue) this.setStateValue(this.element.val(), false, true);
                else if(this.getStateValue() == this.element.val()) this.element.prop('checked','checked');

                //Respond to change event
                this.element.on(event, $.proxy(function(){
                    this.setStateValue(this.element.val());
                }, this));

                //Checks the element on state change
                this.getState().addListener($.proxy(function(name, value){

                    if(value == this.element.val()){
                        this.element.prop('checked',true);
                    }
                }, this), this.options.name);

                return this;
            },

            /**
             * Binds the state to a list element
             * @returns {*}
             */
            list: function(){
                var selector = this.options.selector || '> *',
                    self = this,
                    event = (this.options.event || 'click')+'.state.data-api',
                    value = this.getStateValue();

                if(value === null) value = '';

                //Active current element
                if(this.options.useValue) this.element.find(selector+'[data-state-value='+value+']').addClass('active');

                //Find all list items and add event
                this.element.find(selector).each(function(){
                    $(this).on(event, function(e){
                        e.preventDefault();

                        self.setStateValue($(this).data('stateValue'));
                    });
                });

                //Set element value on state change
                this.getState().addListener($.proxy(function(name, value){

                    value = value || '';
                    this.element.find('.active').removeClass('active');
                    this.element.find(selector+'[data-state-value='+value+']').addClass('active')
                }, this), this.options.name);

                return this;
            },

            /**
             * Binds the state to a button element
             * @returns {*}
             */
            button: function(){

                var event = (this.options.event || 'click')+'.state.data-api';

                //Set the initial state value
                if(this.element.hasClass('active') && this.options.useValue){
                    this.setStateValue(true, false, true);
                } else {
                    if(this.getStateValue()) this.element.addClass('active');
                    else if(!this.element.prop('disabled')) this.setStateValue(false, false, true);
                    else this.setStateValue(null, false, true);
                }

                //Add event to element, toggle state value
                this.element.on(event, $.proxy(function(e){
                    e.preventDefault();

                    this.setStateValue(this.getStateValue() ? false : true);
                }, this));

                //Set element value on state change
                this.getState().addListener($.proxy(function(name, value){

                    if(value === null) this.element.prop('disabled', true);
                    else if(value) this.element.addClass('active').prop('disabled',false);
                    else this.element.removeClass('active').prop('disabled',false);
                }, this), this.options.name);

                return this;
            },

            /**
             * Binds the state to a reset element
             * @returns {*}
             */
            reset: function(){

                var event = (this.options.event || 'click')+'.state.data-api';

                //Get the states to be reset
                var states = this.options.reset ? this.options.reset.split(',') : [];
                var state = this.getState();

                var setDisabledState = $.proxy(function(){
                    var disable = true;
                    for(var key in state.states){

                        if((!states.length || states.indexOf(key) !== -1)){
                            var value = state.get(key);
                            var fallback = state.states[key].fallback;

                            if(value instanceof Array || value instanceof Object){
                                var match = state.Object.equals(value, fallback);
                            }else{
                                var match = value == fallback;
                            }

                            if(!match){
                                disable = false;
                                break;
                            }
                        }
                    }

                    this.element.prop('disabled', disable);
                }, this);

                setDisabledState();

                //Add event to element, toggle state value
                this.element.on(event, $.proxy(function(e){
                    e.preventDefault();

                    //If no specified states, reset all
                    if(!states.length) state.reset();
                    else{
                        for(var i in states){
                            var key = states[i];
                            if(state.has(key)) state.set(key, state.states[key].fallback);
                        }
                    }
                }, this));

                //Set element value on state change
                this.getState().addListener($.proxy(function(name, value){

                    setDisabledState();
                }, this));

                return this;
            },

            /**
             * Binds the state to a standard DOM element
             * @returns {*}
             */
            element: function(){

                var useHtml = this.options.useHtml;

                //Set the initial state value
                if(this.element[useHtml ? 'html' : 'text']() && this.options.useValue) this.setStateValue(this.element[useHtml ? 'html' : 'text'](), false, true);
                else this.element[useHtml ? 'html' : 'text'](this.getStateValue() || '');

                //Set element value on state change
                this.getState().addListener($.proxy(function(name, value){

                    this.element[useHtml ? 'html' : 'text'](value || '');
                }, this), this.options.name);

                return this;
            }
        },

        construct: function(element, options){
            this.element = $(element);
            this.options = $.extend({}, this.options, {name: this.element.prop('name') ? this.element.prop('name').replace(/\[\]$/,'') : null}, options);
            this.tagName = this.element.prop('tagName').toUpperCase();
            this.tagType = this.element.prop('type') ? this.element.prop('type').toUpperCase() : null;

            this.binders = $.extend({}, this.binders, options.binders);

            if(typeof this.options.state == 'string'){
                this.bind();
            }
        },

        /**
         * Gets the state object from the appropriate store. The state name can be provided in a variety of formats
         * #id, .class, [selector] - State is requested from .data('state') of the matching element
         * this - The state is requested from .data('state') of the element
         * $name - The state is requested from $(document).data(name), allows for globally registered states
         * name - The state is requested from window[name], allows for globally registered states
         * @returns {null}
         */
        getState: function(){

            if(!this.state){
                if(this.options.state instanceof State){
                    this.state = this.options.state;

                }else if(this.options.state.match(/^(#|\.|\[)/)){
                    this.state = $(this.options.state).data('state');

                }else if(this.options.state == 'this'){
                    this.state = $(this.element).data('state');

                }else if(this.options.state.match(/^\$/)){
                    this.state = $(document).data(this.options.state.replace(/^\$/,''));

                }else{
                    this.state = window[this.options.state];
                }
            }

            return this.state;
        },

        /**
         * Gets a value from the state for the elements state name
         * @param fallback
         * @returns {*}
         */
        getStateValue: function(fallback){
            return this.getState().get(this.options.name, fallback);
        },

        /**
         * Sets a state value for the elements state name
         * @param value
         * @param callback
         * @returns {*}
         */
        setStateValue: function(value, callback, updateFallback){
            if(!this.options.disableNull && value === '') value = null;
            this.getState().set(this.options.name, value, callback);

            if(updateFallback) this.getState().states[this.options.name].fallback = value;
            return this;
        },

        /**
         * Binds the element events and state events to the element, this function selects the most appropriate bind method
         */
        bind: function(){

            var state = this.getState();
            if(!state) throw new Error('No state object found, check your "state" attribute and that the state object is defined');

            //Ensure state name set
            if(!this.options.name && this.options.reset === null) throw new Error('No state name defined, ensure you supply the state name the element is being bound to');

            //Add state if it doesn't exist
            if(!state.has(this.options.name) && this.options.reset === null) state.insert(this.options.name, null);

            var type = this.options.type || this.tagName;
            if(['RADIO','CHECKBOX','BUTTON','RESET'].indexOf(this.tagType) !== -1) type = this.tagType;
            type = type.toLowerCase();

            var element = this.element, self = this, event = this.options.event;
            switch(type){

                case 'select':
                case 'textarea':
                case 'input':
                    if(this.binders[type]) this.binders[type].call(this);
                    else this.binders.input.call(this);
                    break;

                case 'ul':
                case 'ol':
                    if(this.binders[type]) this.binders[type].call(this);
                    else this.binders.list.call(this);
                    break;

                default:
                    if(this.binders[type]) this.binders[type].call(this);
                    else this.binders.element.call(this);
                    break;
            }
        },

        /**
         * Registers a new binder type
         * @param name
         * @param binder
         * @returns {*}
         */
        addBinder: function(name, binder){
            this.binders[name] = binder;
            return this;
        },

        /**
         * Removes a binder
         * @param name
         * @returns {*}
         */
        removeBinder: function(name){
            delete this.binders[name];
            return this;
        }
    }



    /**
     * jQuery plugin definition
     * @param option
     * @returns {*}
     */
    $.fn.stateBinding = function (option) {

        return this.each(function () {
            var $this = $(this)
                , data = $this.data('stateBinding')
                , options = typeof option == 'object' ? option : {};

            if (!data) $this.data('stateBinding', (data = new StateBinding(this, options)));
            if (typeof option == 'string') data[option]();
        })
    }

    /**
     * Data API
     */
    $(window).on('ready', function () {

        $('[data-state]').each(function () {

            var $this = $(this),
                options = $(this).data();

            //Remove "state" prefix from data attribs
            for(var i in options){
                var value = options[i];
                delete options[i];
                if(i != 'state') i = i.replace(/^state/,'')
                i = i.charAt(0).toLowerCase() + i.substr(1);
                options[i] = value;
            }

            //Delay to allow state variables to become available if needed
            var delay = options.delay;
            if(delay){
                setTimeout(function(){
                    $this.stateBinding(options)
                }, delay)
            }else{
                $this.stateBinding(options)
            }
        })
    })
})(jQuery);