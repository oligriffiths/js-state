//Define state to global
var State;

(function(){

    //Default filters
    var filters = {
        int: function(value){return !isNaN(parseInt(value)) ? parseInt(value) : null},
        float: function(value){return !isNaN(parseFloat(value)) ? parseFloat(value) : null},
        boolean: function(value){return value === true || value === false ? value : null},
        string: function(value){return ['string','boolean','number'].indexOf(typeof value) !== -1 ? value+'' : null},
        array: function(value){return value instanceof Array ? value : null},
        object: function(value){return value instanceof Object ? value : null}
    }

    /**
     * State object
     * @param listener (optional) - listener reference
     * @constructor
     */
    State = function(listener){
        if(listener) this.addListener(listener);
    }

    State.prototype = {

        constructor: State,

        states: {},
        listeners: [],
        Object: {
            /**
             * Returns the size of an object
             * @param obj
             * @returns {number}
             */
            size: function(obj) {
                var size = 0, key;
                for (key in obj) {
                    if (obj.hasOwnProperty(key)) size++;
                }
                return size;
            },

            /**
             * Clones an object
             * @param obj
             * @returns {*}
             */
            clone: function(obj) {
                if (null == obj || "object" != typeof obj) return obj;
                var copy = obj.constructor();
                for (var attr in obj) {
                    if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
                }
                return copy;
            },

            /**
             * Compares 2 objects or arrays to see if they contain the same values
             * @param objectA
             * @param objectB
             * @param strict (default true) - In strict mode, arrays keys must match
             * @returns {boolean}
             */
            equals: function(objectA, objectB, strict){
                var p;

                if(objectA === objectB) return true;

                if(typeof objectA != typeof objectB) return false;

                if(objectA instanceof Array){

                    if(objectA.length != objectB.length) return false;

                    if(strict || strict == undefined){
                        for(p in objectA) if(objectA[p] !== objectB[p]) return false;
                        for(p in objectB) if(objectA[p] !== objectB[p]) return false;
                    }else{
                        for(p in objectA) if(objectB.indexOf(objectA[p]) === -1) return false;
                        for(p in objectB) if(objectA.indexOf(objectB[p]) === -1) return false;
                    }

                }else if(objectA instanceof Object){
                    for(p in objectA) if(objectA.hasOwnProperty(p) && objectA[p] !== objectB[p]) return false;
                    for(p in objectB) if(objectB.hasOwnProperty(p) && objectA[p] !== objectB[p]) return false;
                }

                return true;
            }
        },

        /**
         * Inserts a new state
         * @param name - The state name
         * @param filter - A filter type, defaults supported are int, float, string, array, object
         * @param fallback - Default fallback value, used when the state is reset
         * @param unique - Defines a unique state,
         * @param required - Array of required states, all states must be set in order to be unique
         * @returns {*}
         */
        insert: function(name, filter, fallback, unique, required){

            this.states[name] = {
                filter: filter,
                fallback: typeof fallback !== 'undefined' ? fallback : null,
                value: null,
                unique: typeof unique !== 'undefined' ? unique : false,
                required: required instanceof Array ? required : []
            }

            //Set default value
            if(this.states[name].fallback !== null) this.set(name, this.states[name].fallback, false);

            //Add dynamic name based getter/setter for state
            if(Object.defineProperty && !this.hasOwnProperty(name)){
                Object.defineProperty(this, name, {
                    enumerable: false,
                    get: function(){
                        return this.get(name);
                    },
                    set: function(value){
                        return this.set(name, value);
                    }
                })
            }

            return this
        },

        /**
         * Retrieve a configuration item and return fallback if there is no element set.
         * @param name - The state name
         * @param fallback - The state default value if no state can be found
         * @returns mixed
         */
        get: function(name, fallback){

            if(this.has(name)){
                var value = this.states[name].value;
                if(this.states[name].filter == 'array' && value instanceof Array) return value.slice(0);
                if(this.states[name].filter == 'object' && value instanceof Object) return this.Object.clone(value);
            }else{
                var value = fallback;
            }

            return value;
        },

        /**
         * Set the a state value
         *
         * This function only acts on existing states, if a state has changed it will call back to the model triggering the
         * onStateChange notifier.
         * @param name
         * @param value
         * @returns {*}
         */
        set: function(name, value, callback){
            if(this.has(name)){
                callback = typeof callback === 'undefined' ? true : callback;
                var prev = this.states[name].value;

                //Filter value if not null
                if(value !== null && this.states[name].filter ){
                    var filter = typeof this.states[name].filter == 'function' ? this.states[name].filter : (filters[this.states[name].filter] ? filters[this.states[name].filter] : null);
                    if(filter) value = filter(value);
                }

                var changed = prev !== value;

                //Special case for arrays, comparing values only
                if(prev && ((this.states[name].filter == 'array' && value instanceof Array) || (this.states[name].filter == 'object' && value instanceof Object))) changed = !this.Object.equals(value, prev, false);

                //Only set if value changed
                if(changed){

                    this.states[name].value = value;
                    if(callback) this.triggerListeners(name, value, prev);
                }
            }

            return this;
        },

        /**
         * Check if a state exists
         *
         * @param name - State name
         * @returns {boolean}
         */
        has: function(name){
            return this.states[name] ? true : false;
        },

        /**
         * Remove an existing state
         *
         * @param name
         * @returns {*}
         */
        remove: function(name){
            delete this.states[name];
            return this;
        },

        /**
         * Reset all state data and revert to the default state
         *
         * @param fallback If TRUE use defaults when resetting. Default is TRUE
         * @returns {*}
         */
        reset: function(fallback, callback){
            fallback = typeof fallback !== 'undefined' ? fallback : true;

            for(var i in this.states){
                if(typeof this.states[i] === 'object'){
                    this.set(i, fallback ? this.states[i].fallback : null, callback);
                }
            }

            return this;
        },

        /**
         * Set the state values from an array
         *
         * @param values - An associative array/object of state values by name
         * @returns {*}
         */
        setValues: function(values, callback){

            for(var i in values){
                this.set(i, values[i], callback);
            }

            return this;
        },

        /**
         * Get the state data
         *
         * This function only returns states that have been been set.
         *
         * @param   boolean unique If TRUE only retrieve unique state values, default FALSE
         * @return  array   An associative array/object of state values by name
         * @param unique
         * @returns {{}}
         */
        getValues: function(unique){

            var data = {};
            for(var i in this.states){
                if(this.states.hasOwnProperty(i)){
                    var state = this.states[i];

                    if(state.value !== null){
                        if(unique){

                            if(state.unique){
                                var result = true;

                                //Check related states to see if they're set
                                for(var j in state.required){
                                    if(!this.has(state.required[j]) || !this._validate(this.states[state.required[j]])){
                                        result = false;
                                        break;
                                    }
                                }

                                //Prepare the data to be returned, Include required states
                                if(result){
                                    data[i] = state.value;

                                    for(var j in state.required){
                                        data[j] = this.get(state.required[j]);
                                    }
                                }
                            }
                        }else{
                            data[i] = state.value;
                        }
                    }
                }
            }

            return data;
        },

        /**
         * Check if the state information is unique
         *
         * @return  boolean TRUE if the state is unique, otherwise FALSE.
         */
        isUnique: function(){

            var unique = false;

            var states = this.getValues(true);

            if(this.Object.size(states) > 0){
                unique = true;

                for(var i in states){
                    var state = states[i]
                    if(states.hasOwnProperty(i) && (
                        typeof state === 'object' && this.Object.size(state) > 1 ||
                        state instanceof Array && state.length > 1
                    )){
                        unique = false;
                        break;
                    }
                }
            }

            return unique;
        },

        /**
         * Check if the state information is empty
         *
         * @param   array   $exclude An array of states names to exclude.
         * @return  boolean TRUE if the state is empty, otherwise FALSE.
         */
        isEmpty: function(exclude){

            exclude = exclude || []
            var states = this.getValues()

            for(var i in exclude){
                delete states[i];
            }

            return this.Object.size(states) == 0;

        },

        /**
         * Check if the state information is unique
         *
         * @return  boolean TRUE if the state is unique, otherwise FALSE.
         */
        _validate: function validate(state){
            // Unique values can't be null or empty string.
            if(!state.value && ['number','string'].indexOf(state.value) === -1){
                return false;
            }

            if(state.value instanceof Array){

                //The first element of the array can't be null or empty string
                for(var i in state.value){
                    if(!state.value[i] && ['number','string'].indexOf(typeof state.value) === -1){
                        return false;
                    }
                    break;
                }
            }

            return true;
        },

        /**
         * Adds a listener to be called when the state changes
         * @param listener
         * @param name - Listen only for a specific name
         * @returns {*}
         */
        addListener: function(listener, name){

            if(typeof listener != 'function' && typeof listener.onStateChange != 'function'){
                throw new Error('Bound listeners must be either a function or an object that implements an onStateChange function');
            }

            this.listeners.push({name: name, listener: listener});
            return this;
        },

        /**
         * Removes a listener, either by matching the listener or the name
         * @param listener - Listener function to remove
         * @param name - Removes all listeners for a specific state (listener must be null)
         * @returns {*}
         */
        removeListener: function(listener, name){
            for(var i in this.listeners){
                if(this.listeners[i].listener === listener) delete this.listeners[i];
                else if(this.listeners[i].name == name && !listener) delete this.listeners[i];
            }

            return this;
        },

        /**
         * Triggers the listeners on state change
         * @param name - State being changed
         * @param value - New value
         * @param prev - Old value
         * @returns {*}
         */
        triggerListeners: function(name, value, prev){
            for(var i in this.listeners){
                var listener = this.listeners[i];

                if(!listener) continue;
                else if(listener.name && listener.name != name) continue;
                else if(typeof listener.listener == 'function') listener.listener(name, value, prev, this);
                else if(listener.listener.onStateChange && typeof listener.listener.onStateChange === 'function') listener.listener.onStateChange(name, value, prev, this);
            }

            return this;
        }
    }
})()